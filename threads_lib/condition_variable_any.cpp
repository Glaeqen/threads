#include "condition_variable_any.hpp"

void DETNI::condition_variable_any::notify_one() {
    cv.notify_one();
}

void DETNI::condition_variable_any::notify_all() {
    cv.notify_all();
}
