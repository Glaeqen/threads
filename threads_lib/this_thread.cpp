#include "thread.hpp"
#include "this_thread.hpp"

namespace DETNI {

    void this_thread::interruption_point() {
        auto thread = thread::threads.find(std::this_thread::get_id());
        if (thread != thread::threads.end()) {
            if (thread->second->is_interrupted()) {
                throw thread_interrupted();
            }
        }
    }

}