#ifndef _INTERRUPTABLETHREAD_HPP
#define _INTERRUPTABLETHREAD_HPP

#include <vector>
#include <functional>
#include <mutex>
#include <thread>
#include <map>
#include <condition_variable>
#include "condition_variable_any.hpp"

namespace DETNI {

    class thread {
    public:
        static std::mutex mutex;
        static std::map<std::thread::id, thread *> threads;

        // std::map<void *, to_be_type_of_waiting_conditions*> waiting_conditions;

        thread() = default;

        thread(const thread &) = delete;

        thread(thread &&t) noexcept;

        template<typename _Callable, typename... _Args>
        explicit
        thread(_Callable &&__f, _Args &&... __args) {
            std::mutex m;
            std::unique_lock lock_in_parent_thread(m);
            std::condition_variable initializing_thread;
            this_thread = std::thread([this, &m, &initializing_thread](_Callable &&__f, _Args &&... __args) {
                                          {
                                              std::lock_guard lock_in_child_thread(m);
                                              threads[std::this_thread::get_id()] = this;
                                          }
                                          initializing_thread.notify_one();
                                          try {
                                              __f(std::forward<_Args>(__args)...);
                                          }
                                          catch (...) {}
                                          release_joined_threads();
                                      },
                                      std::forward<_Callable>(__f), std::forward<_Args>(__args)...);
            running = true;
            initializing_thread.wait(lock_in_parent_thread);
        }

        bool is_running() const {
            return running;
        }

        bool is_interrupted() const {
            return interrupted;
        }

        void interrupt();

        std::thread::id get_id() const noexcept;

        bool joinable() const noexcept;

        void join();

        ~thread();

    private:
        std::mutex running_var_mutex;
        condition joined;
        std::thread this_thread;
        bool interrupted = false;
        bool running = false;

        void release_joined_threads();
    };

    class thread_interrupted : public std::exception {
        const char *what() const noexcept override;
    };
}

#endif

