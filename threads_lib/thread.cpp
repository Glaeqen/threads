#include "thread.hpp"


namespace DETNI {
    std::map<std::thread::id, thread *> thread::threads;

    thread::thread(thread &&t) noexcept :
            this_thread(std::forward<std::thread>(t.this_thread)) {
        threads[this_thread.get_id()] = this;
    }

    std::thread::id thread::get_id() const noexcept {
        return this_thread.get_id();
    }

    void thread::interrupt() {
        interrupted = true;
    }

    bool thread::joinable() const noexcept {
        return this_thread.joinable();
    }

    void thread::join() {
        // TODO Move this checkup to some named function
        // If thread from which join() is called is DETNI::Thread
        if (threads.count(std::this_thread::get_id())) {
            std::unique_lock running_lock(running_var_mutex);
            if (!running) return;
            joined.wait(running_lock);
        } else {
            this_thread.join();
        }
    }

    thread::~thread() {
        thread::threads.erase(this_thread.get_id());
    }

    void thread::release_joined_threads() {
        std::lock_guard l(running_var_mutex);
        running = false;
        joined.notify_all();
    }

    const char *thread_interrupted::what() const noexcept {
        return "DETNI::thread_interrupted";
    }
}
