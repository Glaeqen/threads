#ifndef _CONDITION_VARIABLE_ANY_HPP
#define _CONDITION_VARIABLE_ANY_HPP

#include <condition_variable>
#include "this_thread.hpp"

namespace DETNI {
    class condition_variable_any {
    public:
        template<typename Lock>
        void wait(Lock &lock) {
            cv.wait(lock);
            this_thread::interruption_point();
        }

        void notify_one();

        void notify_all();

    private:
        std::condition_variable_any cv;
    };

    using condition = condition_variable_any;
}

#endif
