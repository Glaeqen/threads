#ifndef _THIS_THREAD_HPP
#define _THIS_THREAD_HPP

namespace DETNI {

    class this_thread {

    public:
        static void interruption_point();
    };

}

#endif
