#ifndef _TESTINTERRUPTABLETHREADFIXTURES_HPP
#define _TESTINTERRUPTABLETHREADFIXTURES_HPP

#include <gtest/gtest.h>
#include <vector>
#include <thread.hpp>
#include <this_thread.hpp>
#include <boost/thread.hpp>

template<typename ThreadType>
struct CurrentThreadNamespace {
};

template<>
struct CurrentThreadNamespace<DETNI::thread> {
    using thread_interrupted = DETNI::thread_interrupted;

    struct this_thread {
        constexpr static void (*interruption_point)() = &DETNI::this_thread::interruption_point;
    };
};

template<>
struct CurrentThreadNamespace<boost::thread> {
    using thread_interrupted = boost::thread_interrupted;

    struct this_thread {
        constexpr static void (*interruption_point)() = &boost::this_thread::interruption_point;
    };
};


template<typename T>
struct ThreadTest : public ::testing::Test {
    using ThreadType = T;

    template<typename ...Types>
    ThreadType *constructNewThread(Types &&... args) {
        std::lock_guard l(threadTestMutex);
        auto newThread = new ThreadType(std::forward<Types>(args)...);
        threads.push_back(newThread);
        return newThread;
    }

    std::mutex &constructNewMutex() {
        std::lock_guard l(threadTestMutex);
        auto *newMutex = new std::mutex();
        mutexes.push_back(newMutex);
        return *newMutex;
    }

    void TearDown() override {
        std::lock_guard l(threadTestMutex);
        // Wait for all threads to stop
        for (auto interruptableThread : threads) {
            if (interruptableThread && interruptableThread->joinable()) {
                interruptableThread->join();
            }
        }

        // Now it should be safe to teardown resources
        // Delete threads
        for (auto interruptableThread : threads) {
            delete interruptableThread;
        }
        // Delete mutex
        for (auto mutex : mutexes) {
            delete mutex;
        }
    }

    std::mutex threadTestMutex;
    std::vector<ThreadType *> threads;
    std::vector<std::mutex *> mutexes;
};

#endif
