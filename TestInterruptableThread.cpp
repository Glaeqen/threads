/*
 Boost implements interrupt behaviour for
 * boost::thread::join() 
   > Used 8 times -> Implemented TODO more tests needed
 * boost::thread::timed_join() // DEPRECATED
   > None
 * boost::thread::try_join_for(),
   > None
 * boost::thread::try_join_until(),
   > None
 * boost::condition_variable::wait()
   > None
 * boost::condition_variable::timed_wait()
   > None
 * boost::condition_variable::wait_for()
   > None
 * boost::condition_variable::wait_until()
   > None
   *** boost::condition_variable_any as boost::condition (typedef)
 * boost::condition_variable_any::wait() -> Implemented TODO more tests needed
   > Used 3 times (effectively more -> template)
 * boost::condition_variable_any::timed_wait() -> TODO Not implemented
   > Used 1 time (effectively more -> template)
 * boost::condition_variable_any::wait_for()
   > None
 * boost::condition_variable_any::wait_until()
   > None
 * boost::thread::sleep() > Not implemented // It is done by call boost::this_thread::sleep_for()
   > Used 1 time
 * boost::this_thread::sleep_for() TODO Implement by InterruptableCondition with timed predicate? Boost does it that way
   > Used multiple times
 * boost::this_thread::sleep_until()
   > None
 * boost::this_thread::interruption_point()
   > Used 8 times + -> Implemented TODO more tests needed
*/

#include "TestInterruptableThreadFixtures.hpp"

using namespace std::chrono_literals;

using SpecificDETNIThreadTest = ThreadTest<DETNI::thread>;
using SpecificBoostThreadTest = ThreadTest<boost::thread>;
using ThreadTestTypes = ::testing::Types<DETNI::thread, boost::thread>;

TYPED_TEST_CASE(ThreadTest, ThreadTestTypes);

TYPED_TEST(ThreadTest, ThreadIsDefaultConstructible) {
    ASSERT_EQ(std::is_default_constructible<typename TestFixture::ThreadType>::value, true);
}

TEST_F(SpecificDETNIThreadTest, InterruptableThreadDefaultConstructedIsNotRegistered) {
    auto *t = constructNewThread();
    ASSERT_EQ(DETNI::thread::threads[t->get_id()], nullptr); // Is it correct expectation?
}

TYPED_TEST(ThreadTest, ThreadIsNotCopyConstructible) {
    ASSERT_EQ(std::is_copy_constructible<typename TestFixture::ThreadType>::value, false);
}

TEST_F(SpecificDETNIThreadTest, InterruptableThreadIsProperlyMoveConstructibleWithSwap) {
    auto *t = constructNewThread([]() {});
    std::thread::id originalId = t->get_id();
    ASSERT_EQ(DETNI::thread::threads[originalId], t);
    auto *moved_t = constructNewThread(std::move(*t));
    ASSERT_EQ(moved_t->get_id(), originalId);
    ASSERT_EQ(DETNI::thread::threads[originalId], moved_t);
}

TEST_F(SpecificDETNIThreadTest, InterruptableThreadIsProperlyRegisteredWhenInitialisedWithFunctor) {
    auto *t = constructNewThread([]() {});
    ASSERT_EQ(DETNI::thread::threads[t->get_id()], t);
}

TYPED_TEST(ThreadTest, ExceptionThrownInThreadWhenInterruptedAndInterruptPointSetUp) {
    std::vector<bool> codePartReached;
    auto *t = this->constructNewThread([&codePartReached]() {
        ASSERT_THROW({
                         codePartReached.push_back(true);
                         while (true)
                             CurrentThreadNamespace<typename TestFixture::ThreadType>::this_thread::interruption_point();
                         codePartReached.push_back(true);
                     }, typename CurrentThreadNamespace<typename TestFixture::ThreadType>::thread_interrupted);
    });

    t->interrupt();
    t->join();
    ASSERT_EQ(codePartReached.size(), 1);
}

TYPED_TEST(ThreadTest, ExceptionNotThrownWhenNotInterruptedAndInterruptPointSetUp) {
    std::vector<bool> codePartReached;
    auto *t = this->constructNewThread([&codePartReached]() {
        ASSERT_NO_THROW({
                            codePartReached.push_back(true);
                            CurrentThreadNamespace<typename TestFixture::ThreadType>::this_thread::interruption_point();
                            codePartReached.push_back(true);
                        });
    });

    t->join();
    ASSERT_EQ(codePartReached.size(), 2);
}

TYPED_TEST(ThreadTest, ExceptionThrownInThreadWhenInterruptedAndThreadWaitsAtJoin) {
    std::vector<bool> codePartReached;
    auto *t1 = this->constructNewThread(
            []() {
                std::this_thread::sleep_for(50ms);
            });
    auto *t2 = this->constructNewThread(
            [t1, &codePartReached]() {
                ASSERT_THROW({
                                 codePartReached.push_back(true);
                                 t1->join();
                                 codePartReached.push_back(true);
                             }, typename CurrentThreadNamespace<typename TestFixture::ThreadType>::thread_interrupted);
            });

    t2->interrupt();
    t2->join();
    ASSERT_EQ(codePartReached.size(), 1);
}
