while true
do
  OUTPUT="$(cmake-build-debug/test_threads 2>&1)"
  if [[ "$?" != "0" ]]
  then
    echo $OUTPUT | grep -e ".*where.*" | grep "FAILED" >> /tmp/fails_only.txt
    echo $OUTPUT >> /tmp/whole_error.txt
  fi
done
